package com.half;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

@SpringBootApplication
public class HalfApplication  extends SpringBootServletInitializer {
    //重写配置方法
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(HalfApplication.class);
    }
    public static void main(String[] args) {
        SpringApplication.run(HalfApplication.class,args);
    }
}
