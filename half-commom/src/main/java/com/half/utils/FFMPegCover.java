package com.half.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import springfox.documentation.spring.web.json.Json;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * 该类用于生成上传视频的封面图
 */
public class FFMPegCover {
    private static final Logger LOGGER = LoggerFactory.getLogger(FFMPegCover.class);


    private String ffmpegEXE;

    public FFMPegCover(String ffmpegEXE) {
        this.ffmpegEXE = ffmpegEXE;
    }

    public void convert(String in,String out) throws IOException {
        //ffmpeg.exe -ss 00:00:01 -y -i lex.mp4 -vframes 1 lex.jpg
        List<String> cmd=new ArrayList<String>();
//        cmd.add("/usr/bin/sh");
//        cmd.add("-c");
        cmd.add(ffmpegEXE);
        cmd.add("-ss");
        cmd.add("00:00:01");

        cmd.add("-y");
        cmd.add("-i");

        cmd.add(in);
        cmd.add("-vframes");
       // cmd.add("-strict"); //linux环境下如果截图需要添加下面两个参数
       // cmd.add("-2");
        cmd.add("1");
        cmd.add(out);
        StringBuffer newCmd=new StringBuffer();
        for(int i=0;i<cmd.size();i++) {
            newCmd.append(cmd.get(i) + " ");
            System.out.println(newCmd);
        }
        LOGGER.info("cmdff2",newCmd.toString());
        try {
            String[] strings = {"sh", "-c", newCmd.toString()};
            for (int i = 0; i < strings.length; i++) {
                System.out.println("processInputString"+ strings[i]);
            }
            LOGGER.info("processInputStringLOg",strings);
            //ProcessBuilder processBuilder =new ProcessBuilder(newCmd.toString());
            Process process = Runtime.getRuntime().exec(strings);
        //Process process = processBuilder.start();
            LineNumberReader br = new LineNumberReader(new InputStreamReader(
                    process.getInputStream()));
            StringBuffer sb = new StringBuffer();
            String line;
            while ((line = br.readLine()) != null) {
                System.out.println(line);
                sb.append(line).append("\n");
            }
        LOGGER.info("12312","process"+process.toString());
        System.out.print(process.toString());
        InputStream errorStream = process.getErrorStream();
        InputStreamReader inputStreamReader = new InputStreamReader(errorStream);
        BufferedReader br2 = new BufferedReader(inputStreamReader);

        String line2 = "";
        StringBuffer sb2 = new StringBuffer();
        while ( (line2 = br.readLine()) != null ) {
            sb2.append(line2).append("\n");
        }

        if (br != null) {
            br2.close();
        }
        if (inputStreamReader != null) {
            inputStreamReader.close();
        }
        if (errorStream != null) {
            errorStream.close();
        }}catch (IOException e) {
            e.printStackTrace();
            System.out.println("视频截图失败！");
        }

    }

    public static void main(String[] args) {
        FFMPegCover ffmPegCover = new FFMPegCover("F:\\ffmpeg\\bin\\ffmpeg.exe");
        try {
            ffmPegCover.convert("F:\\ffmpeg\\bin\\lex.mp4","F:\\ffmpeg\\bin\\lex.jpg");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

//    public static void main(String[] args) {
//        FFMPeg ffmPeg =new FFMPeg("F:\\ffmpeg\\bin\\ffmpeg.exe");
//        try {
//            ffmPeg.convert("F:\\ffmpeg\\bin\\test.mp4","F:\\ffmpeg\\bin\\bgm.mp3",4,"F:\\ffmpeg\\bin\\lex.mp4");
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
}
